<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fullpage');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f5E?O/}t*y#;$N=-%.(|YDQGi,aL!vZ99V~Jy4If8+*BEjLx3D`lK%:XqEJ}k Co');
define('SECURE_AUTH_KEY',  '=-k9l )]Q.MDh:id-F[Z2a$|1j=W:n){bCI ?-qL8ok7S3?:OxqP2geC>W+`9G$S');
define('LOGGED_IN_KEY',    '=/u$ZSrk-FDO}c`F9].vxl([PYei|h*M*)Aaql!+UroogW~&7}1N-0=<h[0fF)x7');
define('NONCE_KEY',        'YSd+jIvHYtsBDD^sBHp,}HA:yD)|7)3fj#c&:F|=,1L{QItpSo,{2FL57S{!b&2G');
define('AUTH_SALT',        'C43j#.NGYMX/6c-A ;:Q,E~F-L61[Qr0Ks1ED(6K+YBb f*O.d=l,gdtpX8@9j={');
define('SECURE_AUTH_SALT', '- {w.eC$k=-%y;_*q/!^mljD}!OE|?m,2|]wsxx|zM<kbwyeG|+-U}opzJfIppxs');
define('LOGGED_IN_SALT',   '&|)-E#GJ-OeT~e5tgt; )y9yZcpcSw.g<&Oqn^) >5e|8|Uw@=WnVLj6Nh+=r :2');
define('NONCE_SALT',       '!<LdJa*a4-O9e3*@#1FV!OV34S<,.-pa<<z<^i8[.xx`7P;+M?cXX#W!GiM9]!Tr');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
